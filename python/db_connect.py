import zmq
import struct
from base64 import b16encode
import varints
import logging

log = logging.getLogger(__name__)

COMMAND_GET = 1
COMMAND_SET = 2
COMMAND_DELETE = 3
COMMAND_INC = 4
COMMAND_EXIT = 255

class ClosedException(Exception):
    pass

class ZmqLevelDB(object):
    def __init__(self, address):

        self._context = zmq.Context()
        self._socket = self._context.socket(zmq.REQ)
        self._socket.connect(address)
        self._closed = False

    def _varlen_string(self, value):
        return varints.encode_varint(len(value)) + value

    def _read_varlen_string(self, value):
        for i in range(0, len(value)):
            if value[i] >= 128: break

        i = i + 1

        len_sec = value[0:i]

        str_len = varints.decode_varint(len_sec)
        
        return value[i:i+str_len], value[i+str_len:]
        

    def _send_recv(self, msg):
        if self._closed:
            raise ClosedException()

        log.debug("send:" + b16encode(msg))

        self._socket.send(msg)

        result = self._socket.recv()

        log.debug("recv:" + b16encode(result))

        return result
        

    def pop(self, key, value):
        
        if not isinstance(value, str):
            raise Exception("Value must be a string, not:" + str(type(value)))

        query_msg = chr(COMMAND_SET) + self._varlen_string(key) + self._varlen_string(value)

        result = self._send_recv(query_msg)

        status = ord(result[0])
        
        if status != 1:
            raise Exception("Set status other than 0 returned:%d" % status)

    def get(self, key):

        query_msg = chr(COMMAND_GET) + self._varlen_string(key)

        result = self._send_recv(query_msg)

        status = ord(result[0])
        
        if status == 2:
            return None
        if status == 1:
            value, result = self._read_varlen_string(result[1:])
            return value

    def inc(self, key, by):
        query_msg = chr(COMMAND_INC) + self._varlen_string(key)

        if by >= 0:
            query_msg += chr(0)
        else:
            query_msg += chr(255)

        query_msg += varints.encode_varint(abs(by))

        result = self._send_recv(query_msg)

        status = ord(result[0])

        if status != 1:
            raise Exception("Unexpected status:%d" % status)

        sign = (ord(result[1]) != 0)

        value = varints.decode_varint(result[2:])

        if sign:
            value = value * -1

        return value
        

    def __getitem__(self, key):
        val = self.get(key)

        if val is None:
            raise KeyError()

        return val

    def __setitem__(self, key, value):
        self.pop(key, value)

    def delete(self, key):

        query_msg = chr(COMMAND_DELETE) + self._varlen_string(key)        
       
        result = self._send_recv(query_msg)

        status = ord(result[0])

        if status != 1:
            raise Exception("Delete status other than 0 returned:%d" % status)

    def __delitem__(self, key):
        self.delete(key)

    def __enter__(self):
        return self

    def __exit__(self, tp, value, tb):
        self.close()

    def close(self):
        self._closed = True
        self._socket.close()
        self._context.term()

    def exit_command(self):
        result = self._send_recv(chr(COMMAND_EXIT))

        status = ord(result[0])

        if status != 1 and status != 16:
            raise Exception("Delete status other than 1 returned:%d" % status)

        return status == 1


import unittest
from db_connect import ZmqLevelDB,ClosedException
import logging
from multiprocessing import Pool
import os

def dec(*args, **keyargs):
    pid = os.getpid()

    with ZmqLevelDB("ipc:///tmp/leveldb") as db:
        logging.info("left:%d pid:%d" % (db.inc("abc", -1), pid))

class TestStringMethods(unittest.TestCase):

    def test_single(self):
        with ZmqLevelDB("ipc:///tmp/leveldb") as db:
            del db["abc"], db["abc2"]

            db["abc"] = "cba"
            db["abc2"] = "cba2"
            
            assert db["abc"] == "cba"
            assert db["abc2"] == "cba2"

            del db["abc2"]

            assert db.get("abc2") is None

            def failRead():
                val = db["abc2"]
            
            self.assertRaises(KeyError, failRead)

            del db["abc"], db["abc2"]

            # Test counters
            assert db.inc("abc", 2) == 2

            assert db.inc("abc", -1) == 1
            assert db.inc("abc", -1) == 0

            assert db.inc("abc", 0) == 0

        def readPostClose():
            val = db["abc"]
        
        self.assertRaises(ClosedException, readPostClose)

        with ZmqLevelDB("ipc:///tmp/leveldb") as db:
            db.exit_command()

    def test_mp(self):
        number_of_processes = 10
        
        pool = Pool(number_of_processes)

        try:
            with ZmqLevelDB("ipc:///tmp/leveldb") as db:
                del db["abc"]

                db.inc("abc", number_of_processes)
                pool.map(dec, [True] * number_of_processes)

                assert db.inc("abc", 0) == 0
        finally:
            pool.close()

if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.DEBUG)

    unittest.main()

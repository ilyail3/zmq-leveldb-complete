#include "zmq.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <stdint.h>
#include <syslog.h>

#include "leveldb/db.h"

#include "SocketString.hpp"
#include "Commands.hpp"
#include "VariableLengthInteger.hpp"

int main(){


    leveldb::DB *db;
    leveldb::Options options;
    options.create_if_missing = true;
    leveldb::Status status = leveldb::DB::Open(options, "/tmp/db_dir", &db);

    leveldb::WriteOptions writeOptions;
    leveldb::ReadOptions readOptions;

    void *context = zmq_ctx_new();
    void *socket = zmq_socket(context, ZMQ_REP);

    int rc = zmq_bind(socket, "ipc:///tmp/leveldb");
    assert (rc == 0);

    zmq_msg_t msg;

    unsigned char* data = NULL;
    unsigned char* runner = NULL;
    unsigned char command = 0;

    openlog("leveldb_reader", LOG_PID|LOG_CONS, LOG_USER);

    bool running = true;

    while(running){

        rc = zmq_msg_init (&msg);
        assert (rc == 0);

        syslog(LOG_INFO, "waiting for message\n");

        rc = zmq_msg_recv(&msg, socket, 0);
        assert (rc != -1);

        data = (unsigned char*)zmq_msg_data(&msg);
        command = data[0];

        if(command == LEVELDB_COMMAND_GET){
        	syslog(LOG_INFO, "GET COMMAND\n");

            runner = data+1;
            SocketString* key = SocketString::read(&runner);

            std::string value;

            leveldb::Status status = db->Get(
                readOptions,
                key->slice(),
                &value
            );

            delete key;

            if(status.ok()){
            	syslog(LOG_INFO, "result:%s\n", value.c_str());

                int totalLength = 1 + SocketString::writeSize(&value);
                unsigned char* result = (unsigned char*)malloc(totalLength);

                // Write result status
                result[0] = 1;
                runner = result + 1;

                // Write the result string
                SocketString::write(&runner, &value);

                zmq_send(socket, result, totalLength, 0);

                free(result);

            } else if(status.IsNotFound()){
                zmq_send(socket, "\x02", 1, 0);
            }



        } else if(command == LEVELDB_COMMAND_SET){
        	syslog(LOG_INFO, "SET COMMAND\n");

            runner = data + 1;

            SocketString* key = SocketString::read(&runner);
            SocketString* value = SocketString::read(&runner);


            syslog(LOG_INFO, "key %s(%d)\n", key->data, key->len);
            syslog(LOG_INFO, "value %s(%d)\n", value->data, value->len);


            db->Put(
                writeOptions,
                key->slice(),
                value->slice()
            );

            syslog(LOG_INFO, "write completed\n");

            delete key;
            delete value;

            zmq_send(socket, "\x01", 1, 0);
        } else if(command == LEVELDB_COMMAND_DELETE){
        	syslog(LOG_INFO, "DELETE COMMAND\n");

            runner = data+1;
            SocketString* key = SocketString::read(&runner);

            db->Delete(
                writeOptions,
                key->slice()
            );

            delete key;

            zmq_send(socket, "\x01", 1, 0);
        } else if(command == LEVELDB_COMMAND_INC) {
        	syslog(LOG_INFO, "INC COMMAND\n");

			runner = data+1;

			SocketString* key = SocketString::read(&runner);
			bool positive = (*runner == 0);
			runner = runner + 1;

			uint64_t amount = VariableLengthInteger::read(&runner);

			std::string value;

			leveldb::Status status = db->Get(
				readOptions,
				key->slice(),
				&value
			);



			int64_t number = 0;

			if(status.ok()){
				if(value.length() == sizeof(int64_t))
					number = *((int64_t*) value.c_str());
			}

			if(amount > 0){
				if(positive)
					number += amount;
				else
					number -= amount;

				db->Put(writeOptions, key->slice(), leveldb::Slice((const char*)&number, sizeof(int64_t)));
			}

			delete key;

			positive = number >= 0;
			uint64_t abs_value = abs(number);

			int totalSize =
					1 + // Status
					1 + // Sign
					VariableLengthInteger::size(abs_value); // Abs value;

			char* reply = (char*)malloc(totalSize);

			// Status is 1
			reply[0] = 1;
			reply[1] = positive ? 0 : 255;

			runner = (unsigned char*)(reply + 2);
			VariableLengthInteger::write(abs_value, &runner);

			zmq_send(socket, reply, totalSize, 0);

			free(reply);


#ifdef EXIT_COMMAND
        } else if(command == LEVELDB_COMMAND_EXIT) {
        	running = false;
        	zmq_send(socket, "\x01", 1, 0);
#endif
        } else {
        	syslog(LOG_INFO, "Unknown command:%d", (int)command);

            zmq_send(socket, "\x10", 1, 0);
        }


        //printf("read data:%d\n", *(int*)zmq_msg_data(&msg));
        zmq_msg_close (&msg);
    }

    zmq_close(socket);
    zmq_term(context);

    delete db;
}

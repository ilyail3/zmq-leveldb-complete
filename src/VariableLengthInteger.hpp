/*
 * VariableLengthInteger.hpp
 *
 *  Created on: Apr 27, 2015
 *      Author: ilya
 */

#ifndef VARIABLELENGTHINTEGER_HPP_
#define VARIABLELENGTHINTEGER_HPP_

#include <stdint.h>



class VariableLengthInteger {
public:
	static int size(uint64_t value);
	static void write(uint64_t value, unsigned char** data);
	static uint64_t read(unsigned char** data);
};

#endif /* VARIABLELENGTHINTEGER_HPP_ */

/*
 * VariableLengthInteger.cpp
 *
 *  Created on: Apr 27, 2015
 *      Author: ilya
 */

#include "VariableLengthInteger.hpp"

int VariableLengthInteger::size(uint64_t value){
	int len = 1;

	while (value > 127) {
		value >>= 7;
		len++;
	}

	return len;
}

void VariableLengthInteger::write(uint64_t value, unsigned char** data){
	unsigned char* pos = *data;

	//While more than 7 bits of data are left, occupy the last output byte
	// and set the next byte flag
	while (value > 127) {
		//|128: Set the next byte flag
		*pos = ((unsigned char)(value & 127)) | 128;
		pos = pos+1;
		//Remove the seven bits we just wrote
		value >>= 7;
	}

	*pos = value;

	*data = pos + 1;
}

uint64_t VariableLengthInteger::read(unsigned char** data){
	unsigned char* pos = *data;
	bool done = false;
	uint64_t ret = 0;
	int i = 0;

	while(!done){
		ret |= (*pos & 127) << (7 * i);

		done = ((*pos & 128) == 0);

		// Advance the runner
		i++;
		pos = pos + 1;
	}

	*data = pos;

	return ret;
}

/*
 * SocketString.hpp
 *
 *  Created on: Apr 27, 2015
 *      Author: ilya
 */

#ifndef SOCKETSTRING_HPP_
#define SOCKETSTRING_HPP_

#include <stdint.h>
#include <string>
#include "leveldb/db.h"

class SocketString {
public:
	char* data;
	uint32_t len;

	virtual ~SocketString();

	static SocketString* read(unsigned char** data);
	static int writeSize(std::string* content);
	static void write(unsigned char** runner, std::string* content);

	leveldb::Slice slice();
private:
	SocketString(char* data, uint32_t len);
};

#endif /* SOCKETSTRING_HPP_ */

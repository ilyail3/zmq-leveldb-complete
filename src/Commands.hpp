/*
 * Commands.hpp
 *
 *  Created on: Apr 27, 2015
 *      Author: ilya
 */

#ifndef COMMANDS_HPP_
#define COMMANDS_HPP_

#define LEVELDB_COMMAND_GET 1
#define LEVELDB_COMMAND_SET 2
#define LEVELDB_COMMAND_DELETE 3
#define LEVELDB_COMMAND_INC 4

#ifdef EXIT_COMMAND

#define LEVELDB_COMMAND_EXIT 255

#endif

#endif /* COMMANDS_HPP_ */

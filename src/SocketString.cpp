/*
 * SocketString.cpp
 *
 *  Created on: Apr 27, 2015
 *      Author: ilya
 */

#include "SocketString.hpp"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "VariableLengthInteger.hpp"

SocketString::SocketString(char* data, uint32_t len) {
	// TODO Auto-generated constructor stub
	this->data = data;
	this->len = len;
}

SocketString::~SocketString() {
	free(data);
}

SocketString* SocketString::read(unsigned char** data){
	unsigned char* pos = *data;

	uint64_t len = VariableLengthInteger::read(&pos);

	char* str = (char*)malloc(len + 1);
	memcpy(str, pos, len);
	str[len] = 0;

	*data = pos + len;

	return new SocketString(str, len);
}

int SocketString::writeSize(std::string* content){
	// Size of the varint + size of the string
	return VariableLengthInteger::size(content->length()) + content->length();
}

void SocketString::write(unsigned char** data, std::string* content){
	unsigned char* pos = *data;

	VariableLengthInteger::write(content->length(), &pos);

	memcpy(pos, content->c_str(), content->length());

	*data = pos + content->length();
}

leveldb::Slice SocketString::slice(){
	return leveldb::Slice(
		this->data,
		(size_t)this->len
	);
}

